//
//  Location.swift
//  cta
//
//  Created by Bradley Mirly on 5/8/18.
//  Copyright © 2018 jpmc. All rights reserved.
//

import Foundation

class Location {
    static let baseLocationScale = 0.0055
    
    static func getLocationScale(locationScale: LocationScale) -> Double {
        return baseLocationScale * Double(locationScale.hashValue + 1)
    }
}
