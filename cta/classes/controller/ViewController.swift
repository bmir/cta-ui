//
//  ViewController.swift
//  cta
//
//  Created by Bradley Mirly on 4/25/18.
//  Copyright © 2018 jpmc. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    
    var locationSvc : LocationSvc?
    let location : Location
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        puts("Will appear")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationSvc = LocationSvc(locationDelegate: self)
        location = Location()
        
        locationSvc?.getCurrentLocation() // retrieve current location on app load
        map.showsUserLocation = true // Show Current Location as Blue Circle
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func getLocation(_ sender: Any) {
        locationSvc?.getCurrentLocation()
    }
}

extension ViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocationScale = self.location.getLocationScale()
        
        let span = MKCoordinateSpanMake(0.0055, 0.0055)
        let coordinate = locations[0].coordinate
        let region =  MKCoordinateRegionMake(coordinate, span)
        
        map.setRegion(region, animated: true)
        
        locationSvc?.stopCurrentLocationRequest()
    }
}

